variable "clusterName" {
  description = <<EOF
  The name of the RShift cluster, will be used as a prefix for all aux resources.
  only lowercase alphanumeric characters and hyphens allowed
  EOF
}

variable "chargeCode" {
  description = "The the charge group name to tag resourses"
}

variable "databaseName" {
  description = "The name of the database for the cluster"
}

variable "masterUsername" {
  description = "Name of master user for your cluster"
}

variable "masterPassword" {
  description = "Master password for the database must contain 8 to 64 printable ASCII characters excluding: / \" ' \\ and @.\n It must contain 1 uppercase letter, 1 lowercase letter, and 1 number."
}

variable "nodeTypes" {
  description = "Red Shift node types available"
  type = "list"
  default = [
    "dc1.large",
    "dc1.8xlarge",
    "ds2.xlarge",
    "ds2.8xlarge"]
}

variable "nodeChoice" {
  description = <<EOF
   Node type, input a corresponding number:
   1. dc1.large
   2. dc1.8xlarge
   3. ds2.xlarge
   4. ds2.8xlarge
   EOF
}


variable "clusterTypes"{
  description = "Cluster types available"
  type = "list"
  default = ["single-node", "multi-node"]
}

variable "clusterChoice" {
  description = <<EOF
  Cluster type, input a corresponding number:
  1. single-node
  2. multi-node
  EOF
}

variable "nodesNumber" {
  description = "Number of compute nodes.\n enter 1 for a 'single-node' cluster and form 2 to 32 for a 'multi-node' one"
}