# Valera MANIUK valeramaniuk@protonmail.com
# As a part of a test code for a staffing agency

#The following is for testing purposes only and is not a part of the module

variable "region" {
  default = "us-east-1"
}


provider "aws" {
  region = "${var.region}"

  # My personal acc (Mac/PC)
  profile = "terraform"

}

module "insightCluster" {
  source          = "./modules/RShiftCluster"
  clusterName     = "${var.clusterName}"
  chargeCode      = "${var.chargeCode}"
  databaseName    = "${var.databaseName}"
  masterUsername  = "${var.masterUsername}"
  masterPassword  = "${var.masterPassword}"
  # the list starts at 0 but the menu starts at 1, so '-1'
  nodeType        = "${element(var.nodeTypes, var.nodeChoice -1 )}"
  clusterType     = "${element(var.clusterTypes, var.clusterChoice -1)}"
  nodesNumber     = "${var.nodesNumber}"
}

output "RShift endpoint" {
  value = "${module.insightCluster.RShiftEndpoint}"
}

output "RShift database name" {
  value = "${module.insightCluster.dbName}"
}

output "RShift Master User" {
  value = "${module.insightCluster.dbUser}"
}

output "S3 URL" {
  value = "${module.insightCluster.s3url}"
}

output "S3 ARN" {
  value = "${module.insightCluster.s3arn}"
}

