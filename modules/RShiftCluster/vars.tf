
variable "clusterName" {
  description = "The name of the RShift cluster, will be used as a prefix for all aux resources"
}

variable "chargeCode" {
  description = "The the charge group name to tag resourses"
}

variable "databaseName" {
  description = "The name of the database for the cluster"
}

variable "masterUsername" {
  description = "Name of master user for your cluster"
}

variable "masterPassword" {
  description = "Password must contain 8 to 64 printable ASCII characters excluding: / \" ' \\ and @.\n It must contain 1 uppercase letter, 1 lowercase letter, and 1 number."
}
variable "nodeType" {
  description = ""
}
variable "clusterType"{
  description = "Cluster types available"
}

variable "nodesNumber" {
  description = "Number of compute nodes.\n enter 1 for a 'single-node' cluster and form 2 to 32 for a 'multi-node' one"
}