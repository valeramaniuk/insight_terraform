output "s3arn"{
  value = "${aws_s3_bucket.RShiftBucket.arn}"
}

output "s3url" {
  value = "${aws_s3_bucket.RShiftBucket.bucket_domain_name}"
}

output "RShiftEndpoint" {
  value = "${aws_redshift_cluster.RShiftCluster.endpoint}"
}

output "dbName" {
  value = "${aws_redshift_cluster.RShiftCluster.database_name}"
}

output "dbUser" {
  value = "${aws_redshift_cluster.RShiftCluster.master_username}"
}
