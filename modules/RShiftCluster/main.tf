# Valera MANIUK valeramaniuk@protonmail.com
# As a part of a test code for a staffing agency

# S3 bucket to be used by RShift cluser
# Access restricted to the owner account
resource "aws_s3_bucket" "RShiftBucket" {
  bucket =          "${var.clusterName}-bucket"
  acl    =          "private"

  tags {
    ChargeCode =    "${var.chargeCode}"
    # for personal ref. should be factored out, of course
    Purpose =       "insight global test"
  }
}


# The role for RShift cluster
# starts with ability to assume any role and nothing else
resource "aws_iam_role" "RShiftBucketAccessRole" {
  name = "${var.clusterName}-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "redshift.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}


# Assign the policy allowing access to the specific bucket
# to the role assumed by the RShift cluster
resource "aws_iam_role_policy" "RShiftBucketAccessPolicy" {
  name = "${var.clusterName}-policy"
  role = "${aws_iam_role.RShiftBucketAccessRole.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Action": ["*"],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::${var.clusterName}-bucket/*"
    }
  ]
 }
EOF
}

# The final snapshot will be skipped since it's a test cluster anyways
resource "aws_redshift_cluster" "RShiftCluster" {
  cluster_identifier = "${var.clusterName}"
  database_name      = "${var.databaseName}"
  master_username    = "${var.masterUsername}"
  master_password    = "${var.masterPassword}"
  node_type          = "${var.nodeType}"
  cluster_type       = "${var.clusterType}"
  skip_final_snapshot = true
  iam_roles = ["${aws_iam_role.RShiftBucketAccessRole.arn}"]

  tags {
    ChargeCode       = "${var.chargeCode}"
    Purpose          = "insight global test"
  }
}


